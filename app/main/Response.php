<?php namespace app\main;

class Response
{
    private $data;
    private $status_code = 200;

    public function __construct( $data = null )
    {
        $this->data = $data;
    }

    public function toJson()
    {
        header('Content-Type: application/json');
        $this->data = json_encode( $this->data );
        return $this;
    }

    public function status( $status_code = null )
    {
        if ( ! isset( $status_code ) )
        {
            return $this->status_code;
        }
        $this->status_code = $status_code;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function send()
    {
        http_response_code( $this->status_code );
        echo $this->data;
    }
}





