<?php namespace app\main;

class Request
{
    public $method;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    public function overrideMethod() // :bool
    {
        if ( isset( $_POST['_method'] ) )
        {
            $this->method = $_POST['_method'];
            return true;
        }
        return false;
    }

    public function getMethod()
    {
        switch ($this->method) {
          case 'PUT':
            return 'PUT';
            break;
          case 'POST':
            return 'POST';
            break;
          case 'GET':
            return 'GET';
            break;
          case 'HEAD':
            return 'HEAD';
            break;
          case 'DELETE':
            return 'DELETE';
            break;
          case 'OPTIONS':
            return 'OPTIONS';
            break;
          default:
            throw new \Exception('Undefined request');
            break;
        }
    }

    public function methodIs( $method ) // : bool
    {
       return ( $this->getMethod() === $method ? true : false );
    }

    static public function isXhr()
    {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            {
              return true;
            }
        return false;
    }

    static public function isFile()
    {
        if(isset($_FILES['file']['tmp_name']) ){
            return true;
        }
        return false;
    }

    static public function getJsonData()
    {
        return json_decode(file_get_contents('php://input'), true);
    }
}
