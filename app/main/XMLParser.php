<?php namespace app\main;
/**
*   Parse xml structure and have method for preparing date for printing.
 */
class XMLParser
{
    // raw data
    public $data;

    // Simple xml object
    public $xml = null;

    // xml is valid
    public $load_error = false;

    // if xml not valid this value will be contain an error.
    public $error_data;

    /**
     * Preparing xml structure.
     * @data (string) - xml structure or path to file
     * @isFile (boolean) - if true the @data value contains path to file
     * @return ( void )
     */
    public function __construct($data, $isFile = false)
    {
        libxml_use_internal_errors(true);

       if( $isFile )
       {
           if ( file_exists($data) )
           {
                $this->xml = simplexml_load_file( $data );
           }
           else
           {
               $this->load_error = true;
               $this->error_data = "File not found";
           }
       }
       else
       {
           $this->xml = simplexml_load_string($data);
       }

        if ( ! $this->xml )
        {
            $this->load_error = true;
            $this->error_data = libxml_get_errors();
        }
    }


    /**
     * Method build chain of AirSegment
     * @return (array|false) - return parsed date, or false if xml parsing fails
     */
    public function getAirChain()
    {
        // check xml parsing without error
        if ( $this->load_error )
        {
            return false;
        }
        // Temp val for search max time of travel points and index
        $maxTime = [] ;

        // chain for returning
        $points = [];

        // count of AirSegment
        $counts = count ( $this->xml->AirSegments->AirSegment );

        if ( $counts < 2 ) return false;

        for ( $i = 0;  $i <= $counts-1; $i++  )
        {

            // get board and off points
            $points [$i]['point'] =
                (string)$this->xml->AirSegments->AirSegment[$i]->Board->attributes()['City'] ;

            $points [$i]['board'] =
                (string)$this->xml->AirSegments->AirSegment[$i]->Board->attributes()['Point'] ;

            $points [$i]['off'] =
                (string)$this->xml->AirSegments->AirSegment[$i]->Off->attributes()['Point'] ;

            // calculate time for point
            if ( $i > 0  && $i < $counts-1)
            {
                $date =
                    $this->xml->AirSegments->AirSegment[($i-1)]->Arrival->attributes()['Date'];
                $time =
                    $this->xml->AirSegments->AirSegment[($i-1)]->Arrival->attributes()['Time'];
                $time_one = $date . " " . $time;

                $date =
                    $this->xml->AirSegments->AirSegment[$i]->Departure->attributes()['Date'];
                $time =
                    $this->xml->AirSegments->AirSegment[$i]->Departure->attributes()['Time'];
                $time_two = $date . " " . $time;

                // mark breack if exists.
                if (
                    (string)$this->xml->AirSegments->AirSegment[$i]->Off->attributes()['Point']
                    !==
                    (string)$this->xml->AirSegments->AirSegment[($i+1)]->Board->attributes()['Point']
                ){
                    $points [ $i ]['breack'] = true;
                }

                // calculate max time
                if (
                    ! isset( $maxTime['t'] ) || $maxTime['t']
                    <=
                    (strtotime($time_two) - strtotime($time_one))
                ){
                    $maxTime['t'] =  (strtotime($time_two) - strtotime($time_one));
                    $maxTime['i'] =  $i;
                }
            }
        }

        // set point as end of path
        $points [ $maxTime['i'] ]['end_point'] = true;

        return $points;
    }
}
