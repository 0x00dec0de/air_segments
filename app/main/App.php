<?php namespace app\main;

use app\main\Request;

class App
{
    public $request;

    public function  __construct()
    {
        $this->request = new Request();
    }
}
