<?php

require_once '../app/bootstrap.php';

use app\main\App;
use app\main\Request;
use app\main\Response;
use app\main\XMLParser;

$app = new App();

if ( $app->request->methodIs('GET') )
{
    echo <<<EOF
        <!DOCTYPE html>
        <html lang="en" ng-app="app">
        <head>
            <meta charset="UTF-8">
            <title>test</title>
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
            <link rel="stylesheet" href="https://cdn.rawgit.com/twbs/bootstrap/v4-dev/dist/css/bootstrap.css">
            <link rel="stylesheet" href="/css/app.css">
        </head>
        <body ng-controller="MainController as MC">

        <div class="container" ng-if="MC.error">
        <h2>Error parsing data</h2>
        </div>

        <div class="container" ng-if="MC.dataLoaded">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Full path</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2" ng-repeat="point in MC.dataLoaded"  ng-class="{'text-success' : point.back_path}">
                    {{point.point}}
                    <br>
                    {{point.board}}
                    -
                    {{point.off}}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" ng-if="MC.breacks.length >= 1">
                    <br>
                    <br>
                    <h3>Breaks</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2" ng-repeat="point in MC.breacks">
                    {{point.point}}
                    <br>
                    {{point.board}}
                    -
                    {{point.off}}
                </div>
            </div>
            <div class="row" ng-if="MC.end_point.length >= 1">
                <div class="col-sm-12">
                    <br>
                    <br>
                    <h3>End point</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2"  ng-repeat="point in MC.end_point">
                    {{point.point}}
                    <br>
                    {{point.board}}
                    -
                    {{point.off}}
                </div>
            </div>
        </div>
        <div class="container" ng-if="!MC.dataLoaded">
            <div class="row">
                <div class="col-sm-12 text-sm-center">
                    <br><br>
                    <button class="btn" ng-click="MC.isForm()">Insert Data</button>
                    or
                    <button class="btn" ng-click="MC.isFile()">Upload File</button>
                </div>
            </div>
            <div class="row" ng-switch="MC.action">
                <div class="col-sm-12" ng-switch-when="upload_file">
                    <form>
                         <input type="file" file-model="MC.myFile"/>
                         <button class="btn" ng-click="MC.uploadFile()">Upload</button>
                    </form>
                </div>
                <div class="col-sm-12" ng-switch-when="show_form">
                    <form name="MC.form">
                     <fieldset class="form-group">
                        <label>Please insert a xml structure.</label>
                        <textarea name="MC.text_xml" class="form-control" ng-model="MC.text_xml" rows="10"></textarea>
                        <br>
                        <button class="btn" type="submit" ng-click="MC.submitForm()">Submit</button>
                      </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <script src="/js/app.js"></script>
        </body>
        </html>
EOF;
}
else
{
    if ( $app->request->isFile() )
    {
        if ( file_exists( $_FILES['file']['tmp_name'] ))
        {
            $xmlParObj = new XMLParser( $_FILES['file']['tmp_name'], true );
            unlink( $_FILES['file']['tmp_name']);
        }
        else
        {
            (new Response(['error'=>'File not saved']))->status(500)->toJson()->send();
        }
    }
    else
    {
       $xmlParObj = new XMLParser( $app->request->getJsonData()['data'] );
    }

    if ( $res['data'] = $xmlParObj->getAirChain() )
    {
        (new Response($res))->status(200)->toJson()->send();
    }
    else
    {
        (new Response(['error'=>'Incorect input format!']))->status(500)->toJson()->send();
    }
}
?>

