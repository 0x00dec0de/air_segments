angular.module('app',[])
.controller('MainController',['$http','$scope', 'fileUpload', function($http, $scope, fileUpload){
    vm = this;
    this.breacks =[];
    this.end_point =[];
    this.dataLoaded = false;
    this.isForm = function(){
        this.action = 'show_form';
    };
    this.isFile = function(){
        this.action = 'upload_file';
    };
    function getBreacks(data){
        var triger = false;
        for (i = 0; i < data.length; i++) {

            if ( triger )
            {
                data[i].back_path = true;
            }

            if ( data[i].end_point ){
                triger = true;
            }

            if ( data[i].end_point ){
                vm.end_point.push( data[i] );
            }

            if ( data[i].breack ){
                vm.breacks.push( data[i] );
            }
        }
        return data;
    }
    this.submitForm = function(){
        $http({
            url: '/',
            method: "POST",
            data: { 'data' : this.text_xml }
        }).success( function(res){
            vm.dataLoaded =  getBreacks( res.data );
        })
        .error(function(err){
            vm.error = true;
            console.error( err );
        });
    };

    this.uploadFile = function(){
        if ( this.myFile === undefined ){
            alert('You don\'t select any file!');
            return;
        }
        var file = this.myFile;
        var uploadUrl = "/";
        var reqst = fileUpload.uploadFileToUrl(file, uploadUrl);

        reqst.success(function(res){
            vm.dataLoaded =  getBreacks( res.data );
            console.log(vm.dataLoaded );
        })
        .error(function(err){
            vm.error = true;
            console.error( err );
        });
    };
}])
.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        return $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    };
}])
.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
