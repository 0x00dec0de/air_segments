# Test app

Requirements: php 5.4+, SimpleXML

Document Root for an app will be a /path/to/project/public/ folder.

The app get in input a xml file or a xml structure. Then prints a  Air Segments.
Most of logic app contains in a XMLParser class.

App does not contain strict data validation.
If xml contain not valid structure that need for app - you will be get error.
if a xml contains error of structure and cannot be parsed, you will be informed of this.

As example you can run app using a php-cli

```sh
cd app/public
php -S localhost:8000
```

